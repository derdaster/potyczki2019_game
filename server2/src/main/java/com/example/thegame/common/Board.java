package com.example.thegame.common;

import java.util.Arrays;

public class Board {

	private static final int[][] lines = { /* ROWS */ { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, /* COLS */ { 0, 3, 6 },
			{ 1, 4, 7 }, { 2, 5, 8 }, /* DIAGONALS */ { 0, 4, 8 }, { 2, 4, 6 } };

	private boolean[][] board = newBoard();
	private int boardIndex;

	public int getBoardIndex() {
		return boardIndex;
	}

	public Board(int boardIndex) {
		this.boardIndex = boardIndex;
	}
	
	public Board() {
		new Board(0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}

	public void reset() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board[i][j] = false;
			}
		}
	}

	private boolean[][] newBoard() {
		return new boolean[3][3];
	}

	public void set(int row, int col) {
		checkDimensions(row, col);
		board[row][col] = true;
	}

	public void unSet(int row, int col) {
		checkDimensions(row, col);
		board[row][col] = false;
	}

	private void checkDimensions(int row, int col) {
		if (row >= 3 || col >= 3) {
			throw new IllegalArgumentException("size of board exceeded");
		}
	}

	public boolean get(int row, int col) {
		checkDimensions(row, col);
		return board[row][col];
	}

	public void print() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print((board[i][j] ? "T" : "F") + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public Move findCompletingMove() {
		int index = -1;
		for (int[] line : lines) {
			boolean firstFilled = isPositionFilled(line[0]);
			boolean secondFilled = isPositionFilled(line[1]);
			boolean thirdFilled = isPositionFilled(line[2]);

			if (firstFilled) {
				if (secondFilled) {
					if (!thirdFilled) {
						index = 2;
					}
				} else if (thirdFilled) {
					index = 1;
				}
			} else if (secondFilled && thirdFilled) {
				index = 0;
			}
			if (index != -1) {
				return Move.makeMove(boardIndex, line[index] / 3, line[index] % 3);
			}
		}
		return null;
	}

	public boolean isCenterEmpty() {
		return !board[1][1];
	}

	public boolean isEmpty() {
		return !(board[0][0] || board[0][1] || board[0][2] || board[1][0] || board[1][1] || board[1][2] || board[2][0]
				|| board[2][1] || board[2][2]);
	}

	public boolean isComplete() {
		return anyLineComplete();
	}

	private boolean anyLineComplete() {
		for (int[] line : lines) {
			if (isPositionFilled(line[0]) && isPositionFilled(line[1]) && isPositionFilled(line[2])) {
				return true;
			}
		}
		return false;
	}

	private boolean isPositionFilled(int i) {
		return board[i / 3][i % 3];
	}

	public boolean[][] getBoard() {
		return board;
	}

	public void setCenterEmpty(boolean value) {

	}

	public void setEmpty(boolean value) {

	}

	public void setComplete(boolean value) {

	}

	public int setBoardIndex(int boardIndex) {
		return this.boardIndex = boardIndex;
	}
}
