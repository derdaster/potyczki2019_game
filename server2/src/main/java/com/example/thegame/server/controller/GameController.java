package com.example.thegame.server.controller;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.thegame.common.Board;
import com.example.thegame.common.Move;
import com.example.thegame.server.model.Game;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@RestController
@CrossOrigin(origins = "*")
public class GameController {

	@RequestMapping("/getBoard")
	public List<Board> getStatus() {
		return Game.getBoardList();
	}

	@RequestMapping("/resetBoard")
	public List<Board> resetBoard() {
		Game.resetBoard();
		return Game.getBoardList();
	}

	@RequestMapping("/makeMove")
	@CrossOrigin(origins = "*")
	public List<Board> makeMove(@RequestParam String clientAddress) {
		try {
			Client client = Client.create();

			WebResource webResource = client.resource(clientAddress + "/makeMove");
			ObjectMapper mapper = new ObjectMapper();
			String requestString = mapper.writeValueAsString(Game.getBoardList());
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,
					requestString);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
//			Thread.sleep(100);
			Move move = extractMove(response);
			Game.applyMove(move);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Game.getBoardList();
	}

	private Move extractMove(ClientResponse response) throws IOException, JsonParseException, JsonMappingException {
		String output = response.getEntity(String.class);
		ObjectMapper mapper = new ObjectMapper();

		Move move = mapper.readValue(output, new TypeReference<Move>() {});
		if(Game.isValidMove(move)) {
			return move;
		} else {
			return Game.genRandomMove();
		}
	}

}
