package com.example.thegame.server.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.example.thegame.common.Board;
import com.example.thegame.common.Move;

public class Game {
	private static Random random = new Random();
	private static List<Board> game = initializeGame();
	private static Player currentPlayer = Player.PLAYER1;

	private static List<Board> initializeGame() {
		return generateGame();
	}

	private static List<Board> generateGame() {
		int numberOfBoards = genRandomValue(random, 1, 2);
		return generateGame(numberOfBoards);
	}

	private static List<Board> generateGame(int numberOfBoards) {
		List<Board> game = new ArrayList<>();

		for (int i = 0; i < numberOfBoards; i++) {
			game.add(new Board(i));
		}
		return game;
	}

	private static int genRandomValue(Random random, int minValue, int maxValue) {
		return random.nextInt(maxValue - minValue + 1) + minValue;
	}

	// public static boolean getCoins(Move move) {
	// Integer initialNumberOfElements = game.get(move.getPile());
	// int coins = move.getCoins();
	// if (initialNumberOfElements != null && coins > 0) {
	// initialNumberOfElements -= coins;
	// switchPlayer(currentPlayer);
	//
	// if (initialNumberOfElements >= 0) {
	// board.put(move.getPile(), initialNumberOfElements);
	// return true;
	// } else {
	// return false;
	// }
	// } else {
	// if (move.getCoins() == 0) {
	// board.put(move.getPile(), genRandomValue(new Random(), 1,
	// board.get(move.getPile())));
	// }
	// return false;
	// }
	// }

	private static void switchPlayer(Player player) {
		if (player == Player.PLAYER1) {
			player = Player.PLAYER2;
		} else {
			player = Player.PLAYER1;
		}

	}

	public static Player getCurrentPlayer() {
		return currentPlayer;
	}

	public static void resetBoard() {
		game = initializeGame();
	}

	public static List<Board> getBoardList() {
		return game;
	}

	public static void applyMove(Move move) {
		game.get(move.getBoard()).set(move.getRow(), move.getCol());
		switchPlayer(currentPlayer);
	}

	public static boolean isValidMove(Move move) {
		if (move == null)
			return false;
		boolean isValid = false;
		try {
			Board board = game.get(move.getBoard());
			isValid = !board.get(move.getRow(), move.getCol()) && !board.isComplete();
		} catch (Exception e) {
			return false;
		}
		return isValid;
	}

	public static Move genRandomMove() {
		Board board = game.stream().filter(b -> !b.isComplete()).findFirst().get();

		for (int i = 0; i < 9; i++) {
			int row = i / 3;
			int col = i % 3;
			if (!board.get(row, col)) {
				return Move.makeMove(board.getBoardIndex(), row, col);
			}
		}
		throw new IllegalStateException("Board cannot be full");
	}

}
