package com.example.thegame.client.model;

import java.util.ArrayList;
import java.util.List;

import com.example.thegame.common.Board;

public class Game {
	int numberOfBoards;

	private List<Board> boards = new ArrayList<>();

	public List<Board> getBoards() {
		return boards;
	}

	public Game(int numberOfBoards) {
		this.numberOfBoards = numberOfBoards;
		for (int i = 0; i < numberOfBoards; i++) {
			boards.add(new Board(i));
		}
	}
	
	public boolean isComplete() {
		return boards.stream().allMatch(Board::isComplete);
	}

	public void print() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < boards.size(); j++) {
				Board board = boards.get(j);
				builder.append(tr(board.get(i, 0))).append(" ").append(tr(board.get(i, 1))).append(" ")
						.append(tr(board.get(i, 2))).append("   ");
			}
			builder.append("\r");
		}
		System.out.println(builder.toString());
	}

	public String tr(boolean b) {
		return b ? "T" : "F";
	}

}
