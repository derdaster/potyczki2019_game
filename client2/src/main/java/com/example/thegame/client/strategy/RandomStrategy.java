package com.example.thegame.client.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.example.thegame.common.Board;
import com.example.thegame.common.Move;

public class RandomStrategy implements MoveMaker {

	private List<Integer> indexes = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

	@Override
	public Move makeMove(List<Board> boards) {
		List<Board> nonEmptyBoards = removeEmptyBoards(boards);
		if (nonEmptyBoards.isEmpty()) {
			throw new IllegalArgumentException("Game has already finished");
		}

		Collections.shuffle(nonEmptyBoards);
		return genRandomMove(nonEmptyBoards.get(0));

	}

	private Move genRandomMove(Board board) {

		Collections.shuffle(indexes);
		for (Integer i : indexes) {
			int row = i / 3;
			int col = i % 3;
			if (!board.get(row, col)) {
				return Move.makeMove(board.getBoardIndex(), row, col);
			}
		}
		throw new IllegalStateException("Board cannot be full");

	}

	private List<Board> removeEmptyBoards(List<Board> boards) {
		List<Board> result = new ArrayList<>(boards);
		result.removeIf(Board::isComplete);
		return result;
	}
}
