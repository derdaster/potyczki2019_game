package com.example.thegame.client.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.thegame.client.strategy.MoveMaker;
import com.example.thegame.client.strategy.RandomStrategy;
import com.example.thegame.common.ListOfBoards;
import com.example.thegame.common.Move;

@RestController
@CrossOrigin(origins = "*")
public class Controller {

	private static final MoveMaker moveMaker = new RandomStrategy();

	@RequestMapping("/makeMove")
	public Move makeMove(@RequestBody ListOfBoards board) {
		return moveMaker.makeMove(board);
	}
}
