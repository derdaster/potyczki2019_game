package com.example.thegame.client.strategy;

import java.util.List;

import com.example.thegame.common.Board;
import com.example.thegame.common.Move;

public interface MoveMaker {
	public Move makeMove(List<Board> board);
}
