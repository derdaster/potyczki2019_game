package com.example.thegame.common;

public class Move {
	private int board, row, col;

	public Move(int board, int row, int col) {
		super();
		this.board = board;
		this.row = row;
		this.col = col;
	}
	
	public Move() {
	}

	public static Move makeMove(int board, int row, int col) {
		return new Move(board, row, col);
	}

	public int getBoard() {
		return board;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public void setBoard(int board) {
		this.board = board;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public void setCol(int col) {
		this.col = col;
	}

}
